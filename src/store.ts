import { writable } from 'svelte/store';
import Cookies from 'js-cookie';
import jwtDecode from 'jwt-decode';

export const session = writable(Cookies.get('session'));
export const user = Cookies.get('session')
	? writable(jwtDecode(Cookies.get('session')))
	: writable({ role: 'user' });
